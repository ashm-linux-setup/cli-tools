#!/usr/bin/env bash

function guard_installed() {

    if ! [ -x "$(command -v $1)" ]; then

        echo "Error: $1 not installed" >&2
	exit 1
    fi
}

function tool_installed() {

    if [ -x "$(command -v $1)" ]; then

        return 0
    else

        return 1
    fi
}

if [ -z "$XDG_BIN_HOME" ] || ! [ -d "${XDG_BIN_HOME}" ]; then

    echo "Error: XDG_BIN_HOME not setup" >&2
    exit 1
fi

if [ -z "$ASHM_UTILS_BASHRC" ]; then

    echo "Error: ashm bashrc utils not setup"
    exit 1
fi


guard_installed "cargo"


bashrc_section="cargo-cli-tools"
add_section_to_bashrc "$bashrc_section"

cargo install --locked bat
if tool_installed bat; then

    link_target_directory_name="$(realpath $(dirname $BASH_SOURCE))"
    link_target_file_name="config_bat.sh"
    link_target="${link_target_directory_name}/${link_target_file_name}"
    link_name="${XDG_BIN_HOME}/${link_target_file_name}"

    rm -f "${link_name}"
    ln -s "${link_target}" "${link_name}"
    source_file_in_bashrc_section "$link_target_file_name" "$bashrc_section"
fi

cargo install --locked git-delta   # git diff viewer
cargo install just                 # makefile alternative
cargo install cargo-generate       # template based project generator
