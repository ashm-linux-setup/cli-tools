# Cli tools

Configures the standard cli tools if they have been installed on the system.  The setup script is intended to be called as part of the standard linux environment setup but can be run independently and is intended to be idempotent.


1. Run __setup.sh__

- installs (via symlink) the config/alias scripts into the XDG_BIN_HOME dir if they do not exist
- sources any config/alias script in the bashrc that has not been added
