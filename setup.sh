#!/usr/bin/env bash

script_dir=$(realpath $(dirname "$BASH_SOURCE"))


if [ -z "$XDG_BIN_HOME" ]; then

    echo "Error: XDG directories not set up" >&2
    exit 1
fi

if [ -z "$ASHM_UTILS_REMOTES" ]; then

    echo "Error: ashm remotes utils not set up" >&2
    exit 1
fi

if [ -z "$ASHM_UTILS_BASHRC" ]; then

    echo "Error: ashm bashrc utils not set up" >&2
    exit 1
fi


source "${script_dir}/install_bats.sh"
source "${script_dir}/install_rust_based_cli_tools.sh"
source "${script_dir}/source_shell_aliases_in_bashrc.sh"
