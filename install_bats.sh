#!/usr/bin/env bash
#
# Download and install the bats unit testing framework
# for bash scripts

function error() {

    echo "Bats install [Error]: $1" >&2
}

function has_directory() {

    if ! [ -z "${!1}" ] && [ -d "${!1}" ]; then 

	return 0
    else 

        return 1
    fi
}


if ! has_directory "ASHM_REMOTES_HOME"; then

    error "ASHM_REMOTES_HOME has not been setup"
    return 1
fi

if ! has_directory "XDG_BIN_HOME"; then

    error "XDG_BIN_HOME has not been setup"
    return 1
fi

if [ -z "$ASHM_UTILS_REMOTES" ]; then 

    error "Remotes utils has not bee defined"
    return 1
fi

if [ -z "$ASHM_UTILS_BASHRC" ]; then

    error "Bashrc utils has not been defined"
    return 1
fi


bats_repo="https://github.com/bats-core/bats-core.git"
bats_remote_directory=$(get_remote_directory_path "$bats_repo")

get_or_update_remote "$bats_repo"
"${bats_remote_directory}/install.sh" "${HOME}/.local"

